FROM nginx:1.21.1 AS runtime
EXPOSE 80

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

WORKDIR /wwwroot
# COPY /public/ .