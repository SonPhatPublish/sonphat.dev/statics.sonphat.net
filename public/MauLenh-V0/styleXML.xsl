<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:import href="./templates/temHeader.xsl" />
    <xsl:import href="./templates/temFooter.xsl" />
    <xsl:import href="./templates/lenhvanchuyen/temBody.xsl" />
    <xsl:template name="main" match="/">
        <html lang="en">
            <head>
                <meta charset="UTF-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>Lệnh vận chuyển</title>
                <link rel="stylesheet" href="styleXSLT.css" />
            </head>
            <body>
                <div class="container" style="max-width:650px">
                    <xsl:call-template name="header" />
                    <div class="content">
                        <img>
                            <xsl:attribute name="src">
                                <xsl:value-of select="LenhDienTu/ThongTinKhoiTao/ThongTinDoanhNghiep/Logo" />
                            </xsl:attribute>
                        </img>
                        <xsl:call-template name="body" />
                    </div>
                    <xsl:call-template name="footer" />
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>