<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:import href="../temTokenize.xsl" />
    <xsl:import href="../temQrcode.xsl" />

    <xsl:variable name="dtGiaTriLenhTuNgay" select="LenhDienTu/ThongTinKhoiTao/GiaTriLenhTuNgay" />
    <xsl:variable name="dtGiaTriLenhDenNgay" select="LenhDienTu/ThongTinKhoiTao/GiaTriLenhDenNgay" />
    <xsl:variable name="chuyenDi" select="LenhDienTu/ThongTinKhoiTao/ChuyenDi" />
    <xsl:variable name="benDiXacNhan" select="LenhDienTu/ThongTinBenDiXacNhan" />
    <xsl:variable name="benDenXacNhan" select="LenhDienTu/ThongTinBenDenXacNhan" />

    <xsl:template name="body" match="/">
        <div class="text-align-center font-24 font-bold text-transform-uppercase pt-3">
        Lệnh vận chuyển
    </div>
        <div class="row justify-center">
            <xsl:if test="LenhDienTu/ThongTinKhoiTao/LaLenhTangCuong = 'False'">
                Dùng cho xe ô tô vận chuyển hành khách tuyến cố định
            </xsl:if>
        </div>
        <div class="row justify-center">
            Có giá trị từ ngày:
            <b class="pr-2">
                <xsl:value-of select="concat(
                substring($dtGiaTriLenhTuNgay, 9, 2),
                '/',
                substring($dtGiaTriLenhTuNgay, 6, 2),
                '/',
                substring($dtGiaTriLenhTuNgay, 1, 4)
                )" />
            </b>
            đến ngày:
            <b>
                <xsl:value-of select="concat(
                substring($dtGiaTriLenhDenNgay, 9, 2),
                '/',
                substring($dtGiaTriLenhDenNgay, 6, 2),
                '/',
                substring($dtGiaTriLenhDenNgay, 1, 4)
                )" />
            </b>
        </div>
        <xsl:if test="//*[local-name()='PhuongTienDuocDiThay']">
            <div class="row justify-center">
                <div>
                    <div class="row">
                        Xe ngưng hoạt động tạm thời:
                        <b class="pr-3">....</b>
                        Xe vào thay thế tạm thời:
                        <b>....</b>
                    </div>
                </div>
            </div>
        </xsl:if>
        Loại cấp nốt:
        <xsl:if test="LenhDienTu/ThongTinKhoiTao/LaLenhTangCuong = 'True'">
            <b>Tăng cường</b>
        </xsl:if>
        <xsl:if test="LenhDienTu/ThongTinKhoiTao/LaLenhTangCuong = 'False'">
            <b>Cố định</b>
        </xsl:if>
        <div class="row" style="border:1px solid #000000">
            <div class="xs8">
                <div class="container-xs">
                    <div class="row">
                        <div class="xs2">
                            Cấp cho
                        </div>
                        <div class="xs6">
                            <xsl:for-each select="$chuyenDi/DanhSachLaiXe/LaiXe">
                                <div>
                                    Lái xe
                                    <xsl:value-of select="STT" />
                                    :
                                    <b>
                                        <xsl:value-of select="HoTen" />
                                    </b>
                                </div>
                            </xsl:for-each>
                        </div>
                        <div class="xs4">
                            <xsl:for-each select="$chuyenDi/DanhSachLaiXe/LaiXe">
                                <div>
                                    Hạng GPLX :
                                    <b>
                                        <xsl:value-of select="HangBangLai" />
                                    </b>
                                </div>
                            </xsl:for-each>
                        </div>
                    </div>
                    <div>
                        Nhân viên phục vụ trên xe: 
                    </div>
                    <div class="row">
                        <div class="xs2"></div>
                        <div class="xs5">
                            <xsl:for-each select="$chuyenDi/DanhSachNhanVienPhucVu/NhanVienPhucVu">
                                <div>
                                    Nhân viên
                                    <xsl:value-of select="STT" />
                                    :
                                    <b>
                                        <xsl:value-of select="HoTen" />
                                    </b>
                                </div>
                            </xsl:for-each>
                        </div>
                        <div class="xs5">
                            <xsl:for-each select="$chuyenDi/DanhSachNhanVienPhucVu/NhanVienPhucVu">
                                <div>
                                    Chức vụ :
                                    <b>
                                        <xsl:value-of select="ChucVu" />
                                    </b>
                                </div>
                            </xsl:for-each>
                        </div>
                    </div>
                    <div class="row">
                        <div class="xs5">
                            Biển số đăng ký:
                            <b>
                                <xsl:value-of select="$chuyenDi/ThongTinPhuongTien/PhuongTienThucHien/BienKiemSoat" />
                            </b>
                        </div>
                        <div class="xs3">
                            Số ghế:
                            <b>
                                <xsl:value-of select="$chuyenDi/ThongTinPhuongTien/PhuongTienThucHien/SoGhe" />
                            </b>
                        </div>
                        <div class="xs4">
                            Loại xe:
                            <b>
                                <xsl:value-of select="$chuyenDi/ThongTinPhuongTien/PhuongTienThucHien/LoaiXe" />
                            </b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="xs6">
                            Bến đi, bến đến:
                            <b>
                                <xsl:value-of select="$chuyenDi/TuyenVanChuyen/BenDi/TenBen" />
                                ,
                                <xsl:value-of select="$chuyenDi/TuyenVanChuyen/BenDen/TenBen" />
                            </b>
                        </div>
                        <div class="xs6">
                            Mã số tuyến:
                            <b>
                                <xsl:value-of select="$chuyenDi/TuyenVanChuyen/MaSoTuyen" />
                            </b>
                        </div>
                    </div>
                    <div style="word-wrap: break-word;">
                        Hành trình tuyến:
                        <b>
                            <xsl:value-of select="$chuyenDi/TuyenVanChuyen/HanhTrinhTuyen" />
                        </b>
                    </div>
                </div>
            </div>
            <div class="xs4 text-align-center" style="border-left:1px solid #000000">
                <div class="container-xs">
                    <div>
                        Thủ trưởng đơn vị
                    </div>
                    <div>
                        (Ký xác nhận)
                    </div>
                    <!-- Ký xác nhận Thủ trưởng đơn vị-->
                    <xsl:call-template name="kyLenh">
                        <xsl:with-param name="kyBoi" select="LenhDienTu/DoanhNghiepVanTaiXacNhan//*[local-name()='X509SubjectName']" />
                        <xsl:with-param name="thoiGianKy" select="(LenhDienTu/DoanhNghiepVanTaiXacNhan//*[local-name()='SigningTime'])" />
                    </xsl:call-template>
                </div>
            </div>
        </div>
        <div class="row" style="border:1px solid #000000;border-top:0px solid #000000;">
            <div class="xs8">
                <div class="row" style="height:58px;">
                    <div class="xs5 container-xs px-2 full-height text-align-center" style="border-right:1px solid #000000;box-sizing: border-box;line-height:27px">
                        Bến xe đi, đến
                    </div>
                    <div class="xs5 container-xs px-2 full-height text-align-center" style="border-right:1px solid #000000;box-sizing: border-box;line-height:27px">
                        Giờ xe chạy
                    </div>
                    <div class="xs2 container-xs px-2 full-height text-align-center" style="line-height:27px">
                        Số khách
                    </div>
                </div>
            </div>
            <div class="xs4 text-align-center" style="border-left:1px solid #000000">
                <div class="container-xs">
                    <div>
                        Bến xe
                    </div>
                    <div>
                        (Ký xác nhận)
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="border:1px solid #000000;border-top:0px solid #000000">
            <div class="xs8">
                <div class="row" style="height:78px;">
                    <div class="xs5 container-xs px-2 full-height text-align-center" style="border-right:1px solid #000000;box-sizing: border-box;">
                        Bến xe đi:
                        <xsl:value-of select="$chuyenDi/TuyenVanChuyen/BenDi/TenBen" />
                    </div>
                    <div class="xs5 container-xs px-2 full-height text-align-center" style="border-right:1px solid #000000;box-sizing: border-box;">
                        Xuất bến:
                        <br />
                        <xsl:value-of select="substring($benDiXacNhan/ThoiGianVaoBen, 12, 2)" />
                        h
                        ngày
                        <xsl:value-of select="concat(
                        substring($benDiXacNhan/ThoiGianVaoBen, 9, 2),
                        '/',
                        substring($benDiXacNhan/ThoiGianVaoBen, 6, 2),
                        '/',
                        substring($benDiXacNhan/ThoiGianVaoBen, 1, 4)
                        )" />
                    </div>
                    <div class="xs2 container-xs px-2 text-align-center align-self-center">
                        <xsl:value-of select="LenhDienTu/ThongTinBenDiXacNhan/SoKhachKhiKyLenh" />
                    </div>
                </div>
            </div>
            <div class="xs4 text-align-center" style="border-left:1px solid #000000">
                <xsl:if test="LenhDienTu/ThongTinBenDiXacNhan">
                <!-- Ký xác nhận Bến xe đi-->
                <xsl:call-template name="kyLenh">
                    <xsl:with-param name="kyBoi" select="LenhDienTu/ThongTinBenDiXacNhan//*[local-name()='X509SubjectName']" />
                    <xsl:with-param name="thoiGianKy" select="(LenhDienTu/ThongTinBenDiXacNhan//*[local-name()='SigningTime'])" />
                </xsl:call-template>
                </xsl:if>
            </div>
        </div>
        <div class="row" style="border:1px solid #000000;border-top:0px solid #000000">
            <div class="xs8">
                <div class="row" style="height:78px;">
                    <div class="xs5 container-xs px-2 full-height text-align-center" style="border-right:1px solid #000000;box-sizing: border-box;">
                        Bến xe đến:
                        <xsl:value-of select="$chuyenDi/TuyenVanChuyen/BenDen/TenBen" />
                    </div>
                    <div class="xs5 container-xs px-2 full-height text-align-center" style="border-right:1px solid #000000;box-sizing: border-box;">
                        Đến bến:
                        <br />
                        <xsl:value-of select="substring($benDenXacNhan/ThoiGianVaoBen, 12, 2)" />
                        h
                        ngày
                        <xsl:value-of select="concat(
                        substring($benDenXacNhan/ThoiGianVaoBen, 9, 2),
                        '/',
                        substring($benDenXacNhan/ThoiGianVaoBen, 6, 2),
                        '/',
                        substring($benDenXacNhan/ThoiGianVaoBen, 1, 4)
                        )" />
                    </div>
                    <div class="xs2 container-xs px-2 text-align-center align-self-center">
                        <xsl:value-of select="LenhDienTu/ThongTinBenDenXacNhan/SoKhachKhiKyLenh" />
                    </div>
                </div>
            </div>
            <div class="xs4 text-align-center" style="border-left:1px solid #000000">
                <xsl:if test="LenhDienTu/ThongTinBenDenXacNhan">
                <!-- Ký xác nhận Bến xe đến-->
                <xsl:call-template name="kyLenh">
                    <xsl:with-param name="kyBoi" select="LenhDienTu/ThongTinBenDenXacNhan//*[local-name()='X509SubjectName']" />
                    <xsl:with-param name="thoiGianKy" select="(LenhDienTu/ThongTinBenDenXacNhan//*[local-name()='SigningTime'])" />
                </xsl:call-template>
                </xsl:if>
            </div>
        </div>
        <div class="row" style="border:1px solid #000000;border-top:0px solid #000000">
            <div class="xs8">
                <div class="row" style="height:210px;">
                    <div class="xs4 container-xs px-2 full-height text-align-center" style="border-right:1px solid #000000;box-sizing: border-box;">
                        LÁI XE 1
                        <br />
                        (Xác nhận)
                    </div>
                    <div class="xs4 container-xs px-2 full-height text-align-center" style="border-right:1px solid #000000;box-sizing: border-box;">
                        LÁI XE 2
                        <br />
                        (Xác nhận)
                    </div>
                    <div class="xs4 container-xs px-2 text-align-center">
                        LÁI XE 3
                        <br />
                        (Xác nhận)
                    </div>
                </div>
            </div>
            <div class="xs4 text-align-center" style="border-left:1px solid #000000">
                <div class="container-xs qrcode">
                    <!-- qrcode-->
                    <xsl:call-template name="qrCode">
                        <xsl:with-param name="noidungqr" select="LenhDienTu/ThongTinKhoiTao/qrCode" />
                    </xsl:call-template>
                    <br />
                    <div>
                        (Quét mã QR code để tra cứu thông tin lệnh)
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>