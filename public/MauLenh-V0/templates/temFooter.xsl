<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template name="footer" match="/">
        <div class="container-md px-0 text-align-center" style="border-top:2px dashed #ddd;margin-top:64px">
            <div>
                Đơn vị cung cấp Lệnh điện tử: <b>Công ty CP Tư vấn &amp; Chuyển giao công nghệ Sơn Phát</b> - MST: 4601328480
            </div>
            <div>
                Website: lenhdientu.vn - SDT: 0854888666 - EMail: sonphattn@gmail.com
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>