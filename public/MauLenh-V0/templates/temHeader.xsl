<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:variable name="currentDate" select="LenhDienTu/ThongTinKhoiTao/NgayKhoiTao" />
    <xsl:template name="header" match="/">
        <!-- <div class="font-14 text-align-center">
            (Ban hành kèm theo Thông tư số: 12/2020/TT-BGTVT ngày 29 tháng 5 năm 2020 của Bộ trưởng Bộ Giao thông vận tải)
        </div> -->
        <div class="row justify-space-between">
            <div class="xs-5 text-align-center text-transform-uppercase">
                <div>
                    <xsl:value-of select="LenhDienTu/ThongTinKhoiTao/ThongTinDoanhNghiep/TenDoanhNhiep" />
                </div>
                <div>
                    MST:
                    <xsl:value-of select="LenhDienTu/ThongTinKhoiTao/ThongTinDoanhNghiep/MaSoThueDoanhNghiep" />
                    - SDT:
                    <xsl:value-of select="LenhDienTu/ThongTinKhoiTao/ThongTinDoanhNghiep/SoDienThoai" />
                </div>
                ------------------
                <div>
                    Số:
                    <div class="color-primary" style="display:inline-block">
                        <xsl:value-of select="LenhDienTu/ThongTinKhoiTao/SoLenh" />
                    </div>
                </div>
            </div>
            <div class="xs-7">
                <div class="text-transform-uppercase text-align-center">
                    <div>
                        Cộng hòa xã hội chủ nghĩa Việt Nam
                    </div>
                    <div>
                        Độc lập - Tự do - Hạnh phúc
                    </div>
                    ------------------
                </div>
                
                <div class="text-align-center">
                    Thái Nguyên, ngày
                    <xsl:value-of select="substring($currentDate, 9, 2)" />
                    tháng
                    <xsl:value-of select="substring($currentDate, 6, 2)" />
                    năm
                    <xsl:value-of select="substring($currentDate, 1, 4)" />
                </div>

            </div>
        </div>

    </xsl:template>
</xsl:stylesheet>