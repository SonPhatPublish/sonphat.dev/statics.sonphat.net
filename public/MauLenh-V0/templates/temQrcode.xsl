<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- qr -->
    <xsl:template name="qrCode">
        <xsl:param name="noidungqr" />
        <xsl:element name="img">
            <xsl:attribute name="class">time</xsl:attribute>
            <xsl:attribute name="src">
                https://chart.googleapis.com/chart?chs=200x200&amp;cht=qr&amp;choe=UTF-8&amp;chl=
                <xsl:value-of select="$noidungqr" />
            </xsl:attribute>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>