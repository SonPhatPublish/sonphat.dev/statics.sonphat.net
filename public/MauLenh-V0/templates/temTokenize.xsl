<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- ky lenh -->
    <xsl:template name="kyLenh">
        <xsl:param name="kyBoi" />
        <xsl:param name="thoiGianKy" />
        <div class="container-xs ">
            <div class="kyLenh">
                <img src="images/checkbox-marked-circle-outline.png" alt="" width="24px" />
                <div>
                    <font class="labelBold" style="font-weight:bold;color: #FF0000;word-wrap:break-word">
                        Ký bởi
                        <xsl:call-template name="tokenize">
                            <xsl:with-param name="pText" select="$kyBoi" />
                        </xsl:call-template>
                    </font>
                    <br />
                    <font class="labelBold" style="font-weight:bold;color: #FF0000;word-wrap:break-word">
                        Ký ngày
                        <xsl:value-of select="concat(
                        substring($thoiGianKy, 9, 2),
                        '/',
                        substring($thoiGianKy, 6, 2),
                        '/',
                        substring($thoiGianKy, 1, 4)
                        )" />
                    </font>
                </div>
            </div>
        </div>
    </xsl:template>
    <!-- tokenize-->
    <xsl:template name="tokenize" match="/">
        <xsl:param name="pText" />

        <xsl:if test="string-length($pText)">
            <xsl:choose>
                <xsl:when test="contains($pText,',')">
                    <xsl:variable name="text">
                        <xsl:value-of select="substring-before($pText, ',')" />
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="substring($text,1,3) = 'CN='">
                            <xsl:value-of select="substring-after($text, 'CN=')" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="tokenize">
                                <xsl:with-param name="pText" select="substring-after($pText, ',')" />
                            </xsl:call-template>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="substring($pText,1,3) = 'CN='">
                        <xsl:value-of select="substring-after($pText, 'CN=')" />
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>